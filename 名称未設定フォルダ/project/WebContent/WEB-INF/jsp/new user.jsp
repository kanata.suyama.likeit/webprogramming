<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<div class="header">
		<div class="loginInfo col-2 row ">
			<p class="mr-5">〇〇さん</p>
			<a href="logout">ログアウト</a>
		</div>
	</div>
	<c:if test="${errMsg != null}" >

		  <div class="red">${errMsg}</div>
	</c:if>

	<div class="col-sm-3 mx-auto mt-5">
		<h1>ユーザ新規登録</h1>
	</div>

	<div class="col-4 mx-auto">
	<form action="signup" method="post">
		<table>
			<tr height="100px">
				<td width="200px">ログインID</td>
				<td><input type="text" name="id"></td>
			</tr>
			<tr>
				<td>パスワード</td>
				<td><input type="text" name="pass1"></td>
			</tr>
			<tr height="100px">
				<td width="200px">パスワード確認</td>
				<td><input type="text" name="pass2"></td>
			</tr>
			<tr>
				<td>ユーザ名</td>
				<td><input type="text" name="name"></td>
			</tr>
			<tr height="100px">
				<td width="200px">生年月日</td>
				<td><input type="text" name="day"></td>
			</tr>
		</table>
	</div>
	<div class="col-1 mx-auto mt-5">
		<input type="submit" value="登録">
	</div>
	<div>
		<a class="aaa" href=listservlet>戻る</a>
	</div>
	</form>
</body>
</html>