<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />


</head>
<body>

<c:if test="${errMsg != null}" >

		  <div class="red">${errMsg}</div>
	</c:if>

	<div class="col-sm-3 mx-auto mt-5">
		<h1>ログイン画面</h1>
	</div>
	<form action="user" method="post">
		<div class="col-4 mx-auto">
			<table>
				<tr height="100px">
					<td width="200px">ログインID</td>
					<td><input type="text" name="id"></td>
				</tr>
				<tr>
					<td>パスワード</td>
					<td><input type="text" name="pass"></td>
				</tr>
			</table>
		</div>

		<div class="col-1 mx-auto mt-5">
			<input type="submit" value="ログイン">
		</div>
	</form>
</body>
</html>