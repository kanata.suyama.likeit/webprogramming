<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
      <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
<c:if test="${errMsg != null}" >

		  <p class="red">${errMsg}<p>
	</c:if>

    <div class="col-sm-3 mx-auto mt-5">
        <h1>ユーザ情報更新</h1>
    </div>

    <div class="col-4 mx-auto">

        <table>
          <form action="update" method="post">


            <tr height="100px">
                <td width="200px">ログインID</td>
                <td width="200px">${user.loginId}</td>
                <input type="hidden" name="id" value="${user.id}">
            </tr>
            <tr>
                <td>パスワード</td>
                <td><input type="text" name="pass1" ></td>
            </tr>
            <tr height="100px">
                <td >パスワード確認</td>
                <td><input type="text" name="pass2" ></td>
            </tr>
            <tr>
                <td>ユーザ名</td>
                <td><input type="text" name="name" value="${user.name}"></td>
            </tr>
             <tr height="100px">
                <td >生年月日</td>
                <td><input type="text" name="day" value="${user.birthDate}"></td>
            </tr>


        </table>
    </div>
    <div class="col-1 mx-auto mt-5">
        <input type="submit" value="更新">
    </div>
     </form>
    <div>
        <a href="listservlet">戻る</a>
    </div>
</body>
</html>