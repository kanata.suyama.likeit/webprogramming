<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<form action="listservlet" method="post">

		<div class="header">
			<div class="loginInfo col-2 row ">
				<p class="mr-5"></p>
				<a href="logout">ログアウト</a>
			</div>
		</div>
		<div class="col-sm-3 mx-auto">

			<h1>ユーザー覧</h1>
		</div>
		<div class="loginInfo">
			<a href="signup">新規登録</a>
		</div>
		<div class="col-7 mx-auto">
			<table>
				<tr height="100px">
					<td width="150px">ログインID</td>

					<td><input type="text" name="id" size="50"></td>

				</tr>
				<tr height="100px">
					<td width="150px">ユーザー名</td>

					<td><input type="text" name="name" size="50"></td>
				</tr>

			</table>

	<table>
		<tr height="100px">
			<td width="150px">生年月日</td>

			<td><input type="date" name=day></td>
			<td width="100" align="center">~</td>
			<td><input type="date"name=day1></td>


		</tr>
	</table>
	<div class="col-2 search">
	<form action="listservlet" method="post">
			<input type="submit" value="検索">
			</form>
	</div>
		</form>

	<div class="line"></div>

	</div>
	<div class="col-sm-5 mx-auto mt-5">
		<table border="1" width="500">
			<tr class="abb" bgcolor="#df9b82">
				<th width="500">ログインID</th>
				<th width="500">ユーザー名</th>
				<th width="500">生年月日</th>
				<th></th>
			</tr>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td width="500">
					<a href="detail?id=${user.id}" class="col-3 btn btn-primary">詳細 </a>
					<c:if test="${userInfo.loginId=='admin' or userInfo.loginId==user.loginId}">
					 <a href="update?id=${user.id}" class="col-3 btn btn-primary">更新</a>
					 </c:if>
					 <c:if test="${userInfo.loginId=='admin'}">
					 <a href="delete?id=${user.id}" class="col-3 btn btn-primary">削除</a>
					 </c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>



</body>
</html>