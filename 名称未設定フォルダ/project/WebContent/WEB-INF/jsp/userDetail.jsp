<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
      <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
       <div class="header">
        <div class="loginInfo col-2 row ">
            <p class="mr-5">${userInfo.name}さん</p>
            <a href="logout">ログアウト</a>
        </div>
           </div>
    <div class="col-sm-3 mx-auto mt-5">
        <h1>ログイン画面</h1>
    </div>

    <div class="col-4 mx-auto">
        <table>

            <tr height="100px">
                <td width="300px">ログインID</td>
                <td>${user.loginId}</td>
            </tr>
            <tr>
                <td width="300px">パスワード</td>
                <td>${user.name}</td>
            </tr>
             <tr height="100px">
                <td width="300px">生年月日</td>
                <td>${user.birthDate}</td>
            </tr>
            <tr>
                <td width="300px">登録日時</td>
                <td>${user.createDate}</td>
            </tr>
             <tr height="100px">
                <td width="300px">更新日時</td>
                <td>${user.updateDate}</td>
            </tr>
        </table>


    </div>


        <a href="listservlet">戻る</a>

</body>
</html>