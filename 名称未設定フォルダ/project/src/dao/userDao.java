package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.user;

public class userDao {
	public user login(String id, String pass) {
		Connection conn = null;
		try {
			conn = DB.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);
			pStmt.setString(2, pass);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;

			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			user user = new user(loginIdData, nameData);
			return user;
		}

		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<user> findAll() {
		Connection conn = null;
		List<user> userList = new ArrayList<user>();

		try {

			conn = DB.getConnection();

			String sql = "SELECT * FROM user WHERE id!=1;";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user user = new user(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void signup(String id, String pass, String name, String day) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DB.getConnection();
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,NOW(),NOW())";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			stmt.setString(2, name);
			stmt.setString(3, day);
			stmt.setString(4, pass);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public user detail(String id) {
		Connection conn = null;
		try {
			conn = DB.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginid = rs.getString("login_id");
			String name = rs.getString("name");
			Date day = rs.getDate("birth_date");
			String pass = rs.getString("password");
			String creat = rs.getString("create_date");
			String update = rs.getString("update_date");

			user user = new user(id1, loginid, name, day, pass, creat, update);
			return user;
		}

		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void update(String name, String day, String pass, String id) {
		Connection conn = null;

		try {
			conn = DB.getConnection();
			String sql = "update  user set name=?,birth_date=?,password=? where id=? ";
			PreparedStatement stmt = null;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, day);
			stmt.setString(3, pass);
			stmt.setString(4, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void delete(String id) {
		Connection conn = null;
		try {
			conn = DB.getConnection();
			String sql = "delete from user where id=?";
			PreparedStatement stmt = null;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public user signup(String id) {
		Connection conn = null;
		try {
			conn = DB.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;

			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			user user = new user(loginIdData, nameData);
			return user;
		}

		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String pass(String pass) {

		String source = pass;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			pass = DatatypeConverter.printHexBinary(bytes);
			System.out.println(pass);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return pass;

	}
	public void update2(String id, String name,  String day) {
		Connection conn = null;

		try {
			conn = DB.getConnection();
			String sql = "update  user set name=?,birth_date=? where id=? ";
			PreparedStatement stmt = null;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, day);
			stmt.setString(3, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<user>search(String id1,String name1,String startDate,String endDate) {
		Connection conn = null;
		List<user> userList = new ArrayList<user>();

		try {

			conn = DB.getConnection();

			String sql = "SELECT * from user WHERE id!=1";

			if(!id1.equals("")) {
				sql += " AND login_id = '" + id1 + "'";
			}

			if(!name1.equals("")) {
				sql +=" AND name like '%"+name1+"%'";

			}

			if(! startDate.equals("")) {
				sql+=" AND birth_date >= '"+startDate+"'";

			}
			if(! endDate.equals("")) {
				sql+=" AND birth_date >= '"+endDate+"'";

			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user user = new user(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

}
